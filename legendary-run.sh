#!/bin/bash

if [ $# -ne 1 ]
then
	echo "Usage: legendary-run app-name"
	echo "Check 'legendary list-games' for correct app-name value"
	exit 1
fi

legendary launch "$1" > /dev/null 2>&1

if [ $? -ne 0 ]
then
	yes | \
	legendary update "$1" 2>&1 | \
	awk '/Progress/{gsub(/%/, "", $5); gsub(/[(),]/, "", $6); print $5 "\n#" $6; fflush()}' | \
	zenity --progress --title "Installing..." --text "Installing..." --percentage=0 --auto-close --auto-kill --time-remaining

	legendary launch "$1" > /dev/null 2>&1

	if [ $? -ne 0 ]
	then
		zenity --error --text "Something went wrong.\nManual intervention required." --no-wrap
		exit 1
	fi
fi